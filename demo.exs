Mix.install([
    {:postgrex, ">= 0.0.0"}
])

{:ok, pid} = Postgrex.start_link(
    socket_dir: "/run/postgresql",
    database: "root", # Unfortunatly needed with postgrex 0.16.5 
    # username: "root",
    after_connect: &Postgrex.query!(&1, "SET TIME ZONE 'UTC';", []),
    show_sensitive_data_on_connection_error: true
)

query = Postgrex.prepare!(
    pid,
    "",
    "CREATE TABLE posts (id serial, title text)"
)

Postgrex.close(pid, query)

IO.puts("Bye")

# Postgrex.query!(
#     pid,
#     "SELECT user_id, text FROM comments",
#     []
# )
# %Postgrex.Result{
#     command: :select,
#     empty?: false,
#     columns: ["user_id", "text"],
#     rows: [[3,"hey"],[4,"there"]],
#     size: 2
# }

# Postgrex.query!(
#     pid,
#     "INSERT INTO comments (user_id, text) VALUES (10, 'heya')",
#     []
# )
# %Postgrex.Result{
#     command: :insert,
#     columns: nil,
#     rows: nil,
#     num_rows: 1
# }
